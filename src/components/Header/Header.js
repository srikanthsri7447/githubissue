import React, { Component } from 'react'
import Section from '../Section/Section'
import Spinner from '../Spinner/Spinner'
import './Header.css'

export class Header extends Component {

    constructor(props) {
        super(props)

        this.state = { searchInput: '', issuesArray: [], isLoading: true }
    }

    componentDidMount() {
        fetch('https://api.github.com/repos/expressjs/expressjs.com/issues')
            .then((data) => data.json())
            .then((res) => {
                this.setState({
                    issuesArray: res,
                    isLoading: false
                })
            })
            .catch(console.log)
    }

    onChangingSearchInput = (event) => {
        this.setState({
            searchInput: event.target.value
        })
    }


    render() {

        const { searchInput, issuesArray, isLoading } = this.state

        const resultIssuesArray = issuesArray.filter((eachIssue) => (eachIssue.title.toUpperCase().includes(searchInput.toUpperCase())))

        if (isLoading) {
            return <Spinner />
        }
        else {
            return (
                <div className='header'>
                    <header className='headerContainer'>

                        <img src='https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png'
                            alt='logo'
                            className='logo' />

                        <div className='searchContainer'>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search searchIcon" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                            </svg>
                            <input type='search' className='searchInput' placeholder='Search stories by title' value={searchInput} onChange={this.onChangingSearchInput}></input>
                        </div>

                    </header>
                    <ul className='issuesList'>
                        {resultIssuesArray.map((issue) => {
                            return <Section key={issue.id} issue={issue} />
                        })}

                    </ul>
                </div>
            )
        }

    }
}

export default Header